using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] AudioSource m_AudioSource,m_AudioSourceEffects;
    [SerializeField] float _lerpTime = 0.35f;
    [SerializeField] float _pitchUp,_pitchDown;
    public static SoundManager instance;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
        {
            instance = this;
           
            DontDestroyOnLoad(instance);
        }

    }


   public void PlayOneSound(AudioClip sound)
    {
      
      
        m_AudioSourceEffects.PlayOneShot(sound);
        m_AudioSourceEffects.pitch = Random.Range(_pitchDown, _pitchUp);
    }

    public void PlayMusic (AudioClip music)
    {
        float volumeOriginal = m_AudioSource.volume;
        StartCoroutine(LerpMusic(0, music));


       

    }
    public void play(AudioClip mu) {
        m_AudioSource.clip = mu;
        m_AudioSource.Play();
    }

    IEnumerator LerpMusic(float volumeWanted, AudioClip music)
    {
       float volumeStart= m_AudioSource.volume;
       
        


        while (volumeStart > volumeWanted)
        {
            m_AudioSource.volume -= _lerpTime * Time.deltaTime; 
            volumeStart = m_AudioSource.volume;
          
            yield return null;
        }
        volumeWanted = 1;
        m_AudioSource.clip = music;
        m_AudioSource.Play();
        while (volumeStart<volumeWanted)
            {
               
                m_AudioSource.volume += _lerpTime * Time.deltaTime;
            volumeStart = m_AudioSource.volume;
               
                yield return null;
            }
        

        //StopCoroutine(LerpMusic(volumeWanted, IsGreater));

    }
}
