using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiManger : MonoBehaviour
{
    [SerializeField] Slider m_Slider;
    [SerializeField] float MaxSlider = 120;
    [SerializeField] float Actual =120;
    [SerializeField] float SubtractFlame = 0.001f;
    [SerializeField] float AddFlame;
    [SerializeField] GameObject fade;
    private void OnEnable()
    {
        FlameController.flameTouch += AddingFlame;
    }
    private void OnDisable()
    {
        FlameController.flameTouch -= AddingFlame;
    }
    // Start is called before the first frame update
    void Start()
    {
        Actual = MaxSlider;
        m_Slider.maxValue = MaxSlider;
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       if(Actual > 0) {
            Actual -= SubtractFlame;
            m_Slider.value = Actual;
            fade.transform.localScale = new Vector3(Actual/MaxSlider*100, Actual / MaxSlider * 100, fade.transform.localScale.z);
        }
        else
        {
            SceneManager.LoadScene("Title");
        }
    }

    void AddingFlame()
    {
        Actual += AddFlame;
        if (Actual>MaxSlider)
        {
            Actual= MaxSlider;
        }
    }

}
