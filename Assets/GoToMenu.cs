using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GoToMenu : MonoBehaviour
{
    private void Update()
    {
        if ( Input.GetKeyDown(KeyCode.Escape))
        {
            menu();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            menu();
        }
    }
    public void menu()
    {
        SceneManager.LoadScene("Title");
    }
}
