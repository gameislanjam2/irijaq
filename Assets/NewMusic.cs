using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMusic : MonoBehaviour
{
    [SerializeField] AudioClip musiclip;
    // Start is called before the first frame update
    void Start()
    {
        SoundManager.instance.play(musiclip);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
