using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlat : MonoBehaviour
{
    [SerializeField] float speed;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Rigidbody2D>().transform.position.x <= -7)
        {

            GetComponent<Rigidbody2D>().velocity = new Vector2(1,0)*speed;

        }

        else if (GetComponent<Rigidbody2D>().transform.position.x >= 7) GetComponent<Rigidbody2D>().velocity = new Vector2(-1, 0) * speed;
    }
}
