using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    [SerializeField]AudioClip musicClip;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void changeSong()
    {
        SoundManager.instance.play(musicClip);
    }
    
}
