using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MovementController : MonoBehaviour
{
   

    public PlayerData Data;

    #region Variables
  
    public Rigidbody2D RB { get; private set; }
    private float _fallSpeedYDampingChangeThreshold;
  
    public bool IsFacingRight { get; private set; }
    [SerializeField] public bool IsJumping { get; private set; }
    public bool IsWallJumping { get; private set; }
    public bool IsSliding { get; private set; }
    public bool IsMoving {  get; private set; }
    public float LastOnGroundTime { get; private set; }
    public float LastOnWallTime { get; private set; }
    public float LastOnWallRightTime { get; private set; }
    public float LastOnWallLeftTime { get; private set; }
    public float TimeOnWall=0f;
    public float MaxTimeOnWall=1f;
    //Sounds
    [SerializeField]
    AudioClip[] _audioClips;
   
    [SerializeField] float _timeBetweenWalk=1f;
    [SerializeField] float _resetTimeWalk = 1f;
    //Animator
    [SerializeField] Animator _animator;
    //Particles
    [SerializeField] ParticleSystem _fallParticle,_jumpParticle,_runParticle, _wallJumpParticle, _doubleJumpParticle;
    //Collision
    [SerializeField] BoxControllerCollider _boxFront, _boxBack;
    //Jump
    [SerializeField] private bool _isJumpCut;
    [SerializeField] private bool _IsInGround;
    [SerializeField] private bool _isJumpFalling;
    [SerializeField] int maxJumps = 2;
    [SerializeField] private int howMuchJumpsUsed = 0;
    //Wall Jump
    private float _wallJumpStartTime;
    private int _lastWallJumpDir;

    private Vector2 _moveInput;
    public float LastPressedJumpTime { get; private set; }

    [Header("Checks")]
    [SerializeField] private Transform _groundCheckPoint;
   
    [SerializeField] private Vector2 _groundCheckSize = new Vector2(0.49f, 0.03f);
    [Space(5)]
    [SerializeField] private Transform _frontWallCheckPoint;
    [SerializeField] private Transform _backWallCheckPoint;
    [SerializeField] private Vector2 _wallCheckSize = new Vector2(0.5f, 1f);

    [Header("Layers & Tags")]
    [SerializeField] private LayerMask _groundLayer;
    #endregion

    private void Awake()
    {
        RB = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        SetGravityScale(Data.gravityScale);
        IsFacingRight = true;
        _fallSpeedYDampingChangeThreshold = CameraManager.Instance._fallSpeedYDarpingChangeThreshold;
    }

    private void Update()
    {
        #region TIMERS
        LastOnGroundTime -= Time.deltaTime;
        LastOnWallTime -= Time.deltaTime;
        LastOnWallRightTime -= Time.deltaTime;
        LastOnWallLeftTime -= Time.deltaTime;

        LastPressedJumpTime -= Time.deltaTime;
        #endregion

        #region INPUT HANDLER
        _moveInput.x = Input.GetAxisRaw("Horizontal");
        _moveInput.y = Input.GetAxisRaw("Vertical");

        if (_IsInGround && RB.velocity.x!=0)
        {
            _animator.SetBool("Running", true);
            if (IsMoving)
            {

                IsMoving = false;
                _runParticle.Play();
            }    
        }
        else
        {
            _animator.SetBool("Running", false);
            if (!IsMoving)
            {
                IsMoving = true;
                _runParticle.Stop();
            }
        }

        if (_moveInput.x != 0)
        {
            CheckDirectionToFace(_moveInput.x > 0);
            
            
        }
        
           

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown(KeyCode.J) || Input.GetButtonDown("Jump"))
        {
            OnJumpInput();
        }

        if (Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.C) || Input.GetKeyUp(KeyCode.J) || Input.GetButtonUp("Jump"))
        {
            OnJumpUpInput();
        }
        #endregion

        if (RB.velocity.y < _fallSpeedYDampingChangeThreshold && !CameraManager.Instance.IsLerpingYDamping && !CameraManager.Instance.LerpedFronPlayerFalling)
        {
            CameraManager.Instance.LerYDamping(true);
        }

        if(RB.velocity.y >= 0f && !CameraManager.Instance.IsLerpingYDamping && CameraManager.Instance.LerpedFronPlayerFalling)
        {
            CameraManager.Instance.LerpedFronPlayerFalling = false;
            CameraManager.Instance.LerYDamping(false);
        }

        if(_IsInGround && _timeBetweenWalk > 0 && _moveInput.x!=0) {
            _timeBetweenWalk -= Time.deltaTime;
        }else if(_IsInGround && _moveInput.x != 0)
        {
            _timeBetweenWalk = _resetTimeWalk;
            int clip=Random.Range(0, _audioClips.Length);
            SoundManager.instance.PlayOneSound(_audioClips[clip]);

        }
        #region COLLISION CHECKS

        if(((_boxFront.InCollision)
                    || (_boxBack.InCollision))&&!_IsInGround
                   
                    ){
            _animator.SetBool("Grapping", true);
            RB.position = new Vector2(RB.position.x, RB.position.y - 0.25f * Time.deltaTime);
        }
        if (!IsJumping)
        {
          
            //Ground Check
            if (Physics2D.OverlapBox(_groundCheckPoint.position, _groundCheckSize, 0, _groundLayer) && !IsJumping) 
            {
                LastOnGroundTime = Data.coyoteTime;
                _animator.SetBool("Grapping", false);
               
            }
            else
            {
                TimeOnWall = 0;
                _IsInGround = false;
                _isJumpFalling = true;
                _animator.SetBool("Falling", true);

            }
            
            //Right Wall Check
            if (((_boxFront.InCollision )
                    || (_boxBack.InCollision )) 
                    )
            {
               
               _isJumpFalling = false;
                _isJumpCut =false;
                howMuchJumpsUsed = 0;
                LastOnWallRightTime = Data.coyoteTime;
                
               
                    
               
            }
                
            
               
            //Right Wall Check
            /*if (
                ((_boxFront.InCollision && !IsFacingRight)
                    || (_boxBack.InCollision && IsFacingRight))
                    && !IsWallJumping) 
            {
                
                _animator.SetBool("Grapping", true);
                _isJumpFalling = false;
                _isJumpCut = false;
                howMuchJumpsUsed = 0;
                LastOnWallLeftTime = Data.coyoteTime;
               
                    RB.velocity = new Vector2(RB.velocity.x, RB.velocity.y - 1);
                
            }
               */
            

            LastOnWallTime = Mathf.Max(LastOnWallLeftTime, LastOnWallRightTime);
            
        }
        #endregion

        #region JUMP CHECKS
        if (IsJumping && RB.velocity.y < 0)
        {
            IsJumping = false;

            if (!IsWallJumping)
            {
                
                _isJumpFalling = true;
               

            }
               
        }

        if (IsWallJumping && Time.time - _wallJumpStartTime > Data.wallJumpTime)
        {
            IsWallJumping = false;
        }

        if (LastOnGroundTime > 0 && !IsJumping && !IsWallJumping && !_IsInGround)
        {

            _animator.SetBool("Grapping", false);
            _IsInGround = true;
            _isJumpCut = false;
            howMuchJumpsUsed = 0;
            if (!IsJumping)
            {
               
                _isJumpFalling = false;
                _fallParticle.Play();

            }
                

        }
        if (_isJumpFalling)
        {
            _animator.SetBool("Falling", true);
            _animator.SetBool("Grapping", false);
            
        }
        else
        {
            _animator.SetBool("Falling", false);
        }

        //Jump
        if (CanJump() && LastPressedJumpTime > 0)
        {

            TimeOnWall = 0;
            _animator.SetBool("Grapping", false);
            _animator.SetTrigger("Jumping");
           
            _jumpParticle.Play();
            _IsInGround = false;
            IsJumping = true;
            IsWallJumping = false;
            _isJumpCut = false;
            _isJumpFalling = false;
            howMuchJumpsUsed++;
            Jump();
        }
        //WALL JUMP
        else if (CanWallJump() && LastPressedJumpTime > 0)
        {
            TimeOnWall = 0;
            _animator.SetBool("Grapping", false);
            _animator.SetTrigger("Jumping");
            _IsInGround = false;
            IsWallJumping = true;
            IsJumping = false;
            _isJumpCut = false;
            _isJumpFalling = false;
            _wallJumpStartTime = Time.time;
            _lastWallJumpDir = (LastOnWallRightTime > 0) ? -1 : 1;
            howMuchJumpsUsed++;
            WallJump(_lastWallJumpDir);
        }
        #endregion

        #region SLIDE CHECKS
        if (CanSlide() && ((LastOnWallLeftTime > 0 && _moveInput.x < 0) || (LastOnWallRightTime > 0 && _moveInput.x > 0)))
            IsSliding = true;
        else
            IsSliding = false;
        #endregion

        #region GRAVITY
        //Higher gravity if we've released the jump input or are falling
        if (IsSliding)
        {
            SetGravityScale(0);
        }
        else if (RB.velocity.y < 0 && _moveInput.y < 0)
        {
          
            SetGravityScale(Data.gravityScale * Data.fastFallGravityMult);
          
            RB.velocity = new Vector2(RB.velocity.x, Mathf.Max(RB.velocity.y, -Data.maxFastFallSpeed));
        }
        else if (_isJumpCut)
        {
       
            SetGravityScale(Data.gravityScale * Data.jumpCutGravityMult);
            RB.velocity = new Vector2(RB.velocity.x, Mathf.Max(RB.velocity.y, -Data.maxFallSpeed));
        }
        else if ((IsJumping || IsWallJumping || _isJumpFalling) && Mathf.Abs(RB.velocity.y) < Data.jumpHangTimeThreshold)
        {
            SetGravityScale(Data.gravityScale * Data.jumpHangGravityMult);
        }
        else if (RB.velocity.y < 0)
        {
           
            SetGravityScale(Data.gravityScale * Data.fallGravityMult);
         
            RB.velocity = new Vector2(RB.velocity.x, Mathf.Max(RB.velocity.y, -Data.maxFallSpeed));
        }
        else
        {
            //Default gravity if standing on a platform or moving upwards
            SetGravityScale(Data.gravityScale);
        }
        #endregion
    }

    private void FixedUpdate()
    {
        if (IsWallJumping)
            Run(Data.wallJumpRunLerp);
        else
            Run(1);

        if (IsSliding)
            Slide();
    }

    #region INPUT CALLBACKS
   
    public void OnJumpInput()
    {
        LastPressedJumpTime = Data.jumpInputBufferTime;
        if (_isJumpCut && !CanWallJump() && LastPressedJumpTime > 0) {
            TimeOnWall = 0;
            _animator.SetTrigger("Jumping");
            _doubleJumpParticle.Play();
            IsJumping = true;
            _isJumpCut = false;
            IsWallJumping = false;
            _isJumpFalling = false;
            howMuchJumpsUsed++;
            Jump();
        }
         
    }

    public void OnJumpUpInput()
    {
        
        if ((CanJumpCut() || CanWallJumpCut()) && howMuchJumpsUsed < maxJumps)
            _isJumpCut = true;
       
    }
    #endregion

    #region GENERAL METHODS
    public void SetGravityScale(float scale)
    {
        RB.gravityScale = scale;
    }
    #endregion

    //MOVEMENT METHODS
    #region RUN METHODS
    private void Run(float lerpAmount)
    {
        
        float targetSpeed = _moveInput.x * Data.runMaxSpeed;
       
        targetSpeed = Mathf.Lerp(RB.velocity.x, targetSpeed, lerpAmount);

        #region Calculate AccelRate
        float accelRate;

       
        if (LastOnGroundTime > 0)
            accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? Data.runAccelAmount : Data.runDeccelAmount;
        else
            accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? Data.runAccelAmount * Data.accelInAir : Data.runDeccelAmount * Data.deccelInAir;
        #endregion

        #region Add Bonus Jump Apex Acceleration
       
        if ((IsJumping || IsWallJumping || _isJumpFalling) && Mathf.Abs(RB.velocity.y) < Data.jumpHangTimeThreshold)
        {
            accelRate *= Data.jumpHangAccelerationMult;
            targetSpeed *= Data.jumpHangMaxSpeedMult;
        }
        #endregion

        #region Conserve Momentum
       
        if (Data.doConserveMomentum && Mathf.Abs(RB.velocity.x) > Mathf.Abs(targetSpeed) && Mathf.Sign(RB.velocity.x) == Mathf.Sign(targetSpeed) && Mathf.Abs(targetSpeed) > 0.01f && LastOnGroundTime < 0)
        {
          
            accelRate = 0;
        }
        #endregion

        
        float speedDif = targetSpeed - RB.velocity.x;
       

        float movement = speedDif * accelRate;

        
        RB.AddForce(movement * Vector2.right, ForceMode2D.Force);
        
      
    }

    private void Turn()
    {
        
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;

        IsFacingRight = !IsFacingRight;
    }
    #endregion

    #region JUMP METHODS
    private void Jump()
    {
       
        //Ensures we can't call Jump multiple times from one press
        LastPressedJumpTime = 0;
        LastOnGroundTime = 0;

        #region Perform Jump
      
        float force = Data.jumpForce;
        if (RB.velocity.y < 0)
            force -= RB.velocity.y;

        RB.AddForce(Vector2.up * force, ForceMode2D.Impulse);
        #endregion
    }

    private void WallJump(int dir)
    {
        _wallJumpParticle.Play();
        //Ensures we can't call Wall Jump multiple times from one press
        LastPressedJumpTime = 0;
        LastOnGroundTime = 0;
        LastOnWallRightTime = 0;
        LastOnWallLeftTime = 0;

        #region Perform Wall Jump
        Vector2 force = new Vector2(Data.wallJumpForce.x, Data.wallJumpForce.y);
        force.x *= dir; //apply force in opposite direction of wall

        if (Mathf.Sign(RB.velocity.x) != Mathf.Sign(force.x))
            force.x -= RB.velocity.x;

        if (RB.velocity.y < 0) 
            force.y -= RB.velocity.y;


        RB.AddForce(force, ForceMode2D.Impulse);
        #endregion
       
    }
    #endregion

    #region OTHER MOVEMENT METHODS
    private void Slide()
    {

        float speedDif = Data.slideSpeed - RB.velocity.y;
        float movement = speedDif * Data.slideAccel;
        
        movement = Mathf.Clamp(movement, -Mathf.Abs(speedDif) * (1 / Time.fixedDeltaTime), Mathf.Abs(speedDif) * (1 / Time.fixedDeltaTime));

        RB.AddForce(movement * Vector2.up);
    }
    #endregion


    #region CHECK METHODS
    public void CheckDirectionToFace(bool isMovingRight)
    {
        if (isMovingRight != IsFacingRight)
            Turn();
    }

    private bool CanJump()
    {
        return LastOnGroundTime > 0 && !IsJumping ;
    }

    private bool CanWallJump()
    {
        return LastPressedJumpTime > 0 && LastOnWallTime > 0 && LastOnGroundTime <= 0 && (!IsWallJumping ||
             (LastOnWallRightTime > 0 && _lastWallJumpDir == 1) || (LastOnWallLeftTime > 0 && _lastWallJumpDir == -1));
    }

    private bool CanJumpCut()
    {
        return IsJumping && RB.velocity.y > 0;
    }

    private bool CanWallJumpCut()
    {
        return IsWallJumping && RB.velocity.y > 0;
    }

    public bool CanSlide()
    {
        if (LastOnWallTime > 0 && !IsJumping && !IsWallJumping && LastOnGroundTime <= 0)
            return true;
        else
            return false;
    }
    #endregion


    #region EDITOR METHODS
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(_groundCheckPoint.position, _groundCheckSize);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(_frontWallCheckPoint.position, _wallCheckSize);
        Gizmos.DrawWireCube(_backWallCheckPoint.position, _wallCheckSize);
    }
    #endregion
}
