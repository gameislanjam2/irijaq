using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpSoundController : MonoBehaviour
{
    [SerializeField] AudioClip _jumpSound;

    public void SoundPlay()
    {
        SoundManager.instance.PlayOneSound( _jumpSound );
    }
}
