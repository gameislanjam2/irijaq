using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ParallaxMotion : MonoBehaviour
{

    [SerializeField] float ParallaxSpeed;
    [SerializeField] GameObject jugador;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.RightArrow)) gameObject.GetComponent<SpriteRenderer>().material.mainTextureOffset += Vector2.right*Time.deltaTime * ParallaxSpeed;
        if (Input.GetKey(KeyCode.LeftArrow)) gameObject.GetComponent<SpriteRenderer>().material.mainTextureOffset -= Vector2.right * Time.deltaTime * ParallaxSpeed;

        if (jugador.IsDestroyed()) { ParallaxSpeed = 0; }


    }
}
