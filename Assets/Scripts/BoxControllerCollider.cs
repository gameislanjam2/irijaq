using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxControllerCollider : MonoBehaviour
{
    
    public bool InCollision;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 6)
        {
            InCollision = true;
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 6)
        {
            InCollision = false;
        }
    }
    

}
