using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CameraManager : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera[] _allVirtualCameras;
    public static CameraManager Instance;
    [SerializeField] float _fallPanAmount= 0.25f;
    [SerializeField] float _fallYPanTime = 0.35f;
    public float _fallSpeedYDarpingChangeThreshold = -15f;

    public bool IsLerpingYDamping { get; private set; } 
    public bool LerpedFronPlayerFalling {  get; set; }
    private Coroutine _lerpYPanCorputine;
    private CinemachineVirtualCamera _currentCamera;
    private CinemachineFramingTransposer _framingTransporser;
    private float _normYPanAmount;
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        
        for(int i = 0; i<_allVirtualCameras.Length; i++)
        {
            if (_allVirtualCameras[i].enabled)
            {
                _currentCamera = _allVirtualCameras[i];
                _framingTransporser = _currentCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
            }
        }
        _normYPanAmount = _framingTransporser.m_YDamping;
    }

    public void LerYDamping(bool isPlayerFalling) { 
        _lerpYPanCorputine=StartCoroutine(LerpYAction(isPlayerFalling));
    }

    private IEnumerator LerpYAction(bool isPlayerFalling) {
        IsLerpingYDamping = true;
        float starDampAmount = _framingTransporser.m_YDamping;
        float endDampAmount = 0f;

        if (isPlayerFalling) {
            endDampAmount = _fallPanAmount;
            LerpedFronPlayerFalling=true;
        } else
        {
            endDampAmount = _normYPanAmount;
        }
        float elapsedTime = 0f;
        while (elapsedTime < _fallYPanTime)
        {
            elapsedTime += Time.deltaTime;
            float lerpedPanAmount = Mathf.Lerp(starDampAmount,endDampAmount,(elapsedTime/ _fallYPanTime));

            yield return null;
        }
        IsLerpingYDamping = false;
    }
}
