using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatController : MonoBehaviour
{
    bool touched = false;
    float delay = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (touched) {

            delay -= Time.deltaTime;
            if (delay <= 0) {
                
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                GetComponent<Rigidbody2D>().gravityScale = 0.8f;
                GetComponent<Rigidbody2D>().freezeRotation = true;
                
            }

        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision != null && !touched)
        {
            if (collision.collider.CompareTag("Player")) {

                Debug.Log("Player detected!!");
                //GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                touched = true;
            
            }
        }
    }

    private void OnBecameInvisible()
    {
        GetComponent<Rigidbody2D>().transform.position = Vector2.zero;
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        touched = false;
        delay = 1;
    }
}
