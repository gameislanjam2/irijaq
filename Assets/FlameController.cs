using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FlameController : MonoBehaviour
{
   public static Action flameTouch;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag =="Player")
        {
            flameTouch?.Invoke();
            Destroy(gameObject);
        }
    }
}
